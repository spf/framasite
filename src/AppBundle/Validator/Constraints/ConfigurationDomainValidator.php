<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Controller\BuyDomains\DomainCommandController;
use AppBundle\Entity\Domain;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Narno\Gandi\Api as GandiAPI;

class ConfigurationDomainValidator extends ConstraintValidator
{

    /** @var Constraint */
    private $constraint;

    /** @var Domain */
    private $domainObject;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var GandiAPI
     */
    private $api;

    /**
     * ConfigurationDomainValidator constructor.
     * @param LoggerInterface $logger
     * @param $apiMode
     * @param $apiKeyProd
     * @param $apiKeyTest
     */
    public function __construct(LoggerInterface $logger, $apiMode, $apiKeyProd, $apiKeyTest)
    {
        $this->api = new GandiAPI($apiMode !== 'prod');
        // set API key
        $this->apiKey = $apiKeyTest;
        if ($apiMode === 'prod') {
            $this->apiKey = $apiKeyProd;
        }
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param Domain $domainObject The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($domainObject, Constraint $constraint)
    {
        $this->domainObject = $domainObject;
        $this->constraint = $constraint;
        if ($domainObject->isRegisteredByFrama()) {
            $this->validateDomainName();
        } else {
            $this->validateDnsZone();
        }
    }

    private function validateDnsZone()
    {
        $domain = $this->domainObject->getDomainName();
        $this->validateDnsZoneAction($domain);
    }

    private function validateDnsZoneAction($domain)
    {
        $cname = dns_get_record($domain, DNS_CNAME);
        if ($cname && isset($cname[0]) && $cname[0]['type'] === 'CNAME') {
            $this->validateDnsZoneAction($cname[0]['target']);
        }
        $ipv4 = dns_get_record($domain, DNS_A);
        $ipv6 = dns_get_record($domain, DNS_AAAA);

        if (!$ipv4) {
            $this->context->buildViolation($this->constraint->missingDomainMessage)
                ->setParameter('%domain%', $domain)
                ->setParameter('%ip%', 4)
                ->addViolation();
        }

        if (!$ipv6) {
            $this->context->buildViolation($this->constraint->missingDomainMessage)
                ->setParameter('%domain%', $domain)
                ->setParameter('%ip%', 6)
                ->addViolation();
        }

        if ($ipv4 && dns_get_record("frama.site", DNS_A)[0]['ip'] !== $ipv4[0]['ip']) {
            $this->context->buildViolation($this->constraint->incorrectDomainMessage)
                ->setParameter('%domain%', $domain)
                ->setParameter('%ip%', 4)
                ->setParameter('%record%', 'A')
                ->addViolation();
        }

        if ($ipv6 && dns_get_record("frama.site", DNS_AAAA)[0]['ipv6'] !== $ipv6[0]['ipv6']) {
            $this->context->buildViolation($this->constraint->incorrectDomainMessage)
                ->setParameter('%domain%', $domain)
                ->setParameter('%ip%', 6)
                ->setParameter('%record%', 'AAAA')
                ->addViolation();
        }
    }

    private function validateDomainName()
    {
        $domain = $this->domainObject->getDomainName();

        $domainWithExtension = explode('.', $domain, 2);
        if ($domain === $domainWithExtension[0]) {
            $this->context->buildViolation($this->constraint->domainWithoutExtension)
                ->setParameter('%domain%', $domain)
                ->addViolation();
            return;
        }
        $extension = end($domainWithExtension);
        if (!in_array($extension, DomainCommandController::OFFERED_EXTENSIONS)) {
            $this->context->buildViolation($this->constraint->domainWithExtensionNotSupported)
                ->setParameter('%extension%', $extension)
                ->addViolation();
        }

        $availabilities = $this->api->domain->available([$this->apiKey, [$domain]]);
        while ($availabilities[$domain] === 'pending') {
            $availabilities = $this->api->domain->available([$this->apiKey, [$domain]]);
        }

        if ($availabilities[$domain] !== 'available') {
            $this->context->buildViolation($this->constraint->unavailableDomainName)
                ->setParameter('%domain%', $this->domainObject->getDomainName())
                ->setParameter('%return%', $availabilities[$domain])
                ->addViolation();
        }
    }
}
