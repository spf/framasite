<?php

namespace AppBundle\Service\Email;

use AppBundle\Entity\Command;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Translation\TranslatorInterface;

class InvoiceService
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var string
     */
    private $fromEmail;

    /**
     * @var string
     */
    private $framasoftAddress;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var EngineInterface
     */
    private $engine;

    /**
     * InvoiceService constructor.
     * @param TranslatorInterface $translator
     * @param \Swift_Mailer $mailer
     * @param EngineInterface $engine
     * @param string $fromEmail
     * @param string $framasoftAddress
     */
    public function __construct(
        TranslatorInterface $translator,
                                \Swift_Mailer $mailer,
                                EngineInterface $engine,
                                string $fromEmail,
                                string $framasoftAddress
    ) {
        $this->translator = $translator;
        $this->mailer = $mailer;
        $this->engine = $engine;
        $this->fromEmail = $fromEmail;
        $this->framasoftAddress = $framasoftAddress;
    }

    public function sendInvoice(Command $command)
    {
        $message = new \Swift_Message($this->translator->trans('command.receipt.title'));
        $message->setFrom($this->fromEmail)
            ->setTo($command->getUser()->getEmail())
            ->setBody(
                $this->engine->render(
                    'default/command/invoice.html.twig',
                    ['command' => $command, 'framasoftAddress' => $this->framasoftAddress]
                ),
                'text/html'
            );

        $this->mailer->send($message);
    }
}
