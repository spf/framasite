<?php

namespace AppBundle\Service\Domain;

use AppBundle\Async\AsyncAccountDoku;
use AppBundle\Async\AsyncAccountPrettyNoemie;
use AppBundle\Entity\Certificate\CertData;
use AppBundle\Entity\Certificate\CertTask;
use AppBundle\Entity\Domain;
use AppBundle\Entity\SinglePage\SinglePage;
use AppBundle\Entity\Site;
use AppBundle\Entity\User;
use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Exception\APIException\Attach\AttachDomainException;
use AppBundle\Exception\SiteException\UnknownSiteType;
use AppBundle\Helper\CertTaskFactory;
use AppBundle\Service\BaseService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AttachDomainService extends BaseService
{
    /**
     * @var CertTaskFactory
     */
    private $certTaskFactory;

    /**
     * @var AsyncAccountDoku
     */
    private $accountDoku;

    /**
     * @var AsyncAccountPrettyNoemie
     */
    private $accountPrettyNoemie;

    /**
     * DetachDomainService constructor.
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     * @param CertTaskFactory $certTaskFactory
     * @param AsyncAccountDoku $accountDoku
     * @param AsyncAccountPrettyNoemie $accountPrettyNoemie
     */
    public function __construct(
        EntityManagerInterface $entityManager,
                                LoggerInterface $logger,
                                CertTaskFactory $certTaskFactory,
                                AsyncAccountDoku $accountDoku,
                                AsyncAccountPrettyNoemie $accountPrettyNoemie
    ) {
        parent::__construct($logger, $entityManager);
        $this->certTaskFactory = $certTaskFactory;
        $this->accountDoku = $accountDoku;
        $this->accountPrettyNoemie = $accountPrettyNoemie;
    }

    /**
     * @param Domain $domain
     * @param User $user
     * @return Site
     * @throws UnknownSiteType
     */
    public function attachDomain(Domain $domain, User $user): Site
    {
        $domain->setStatus(Domain::DOMAIN_WAITING_CONFIGURATION);
        $site = $domain->getSite();

        if ($this->entityManager->getRepository('AppBundle:Domain')->findOneBy(['domainName' => $domain->getDomainName()])) {
            throw new AccessDeniedException('This domain is already attached');
        }

        $this->entityManager->persist($domain);
        $this->entityManager->persist($site);
        $this->entityManager->flush();
        $this->logger->info('Attached domain ' . $domain->getDomainName() . ' to site ' . $site->getSubdomain() . ' by user' . $user->getUsername());

        if ($site instanceof Wiki) {
            $this->logger->info('Since site ' . $site->getSubdomain() . " is a wiki, we need to link it's domain");
            $this->accountDoku->setSite($site)->setUser($user)->setLogger($this->logger);
            $this->accountDoku->linkDomainFolder($site->getSubdomain(), $domain->getDomainName());
        } elseif ($site instanceof SinglePage) {
            $this->logger->info('Since site ' . $site->getSubdomain() . " is a single page, we need to link it's domain");
            $this->accountPrettyNoemie->setSite($site)->setUser($user)->setLogger($this->logger);
            $this->accountPrettyNoemie->linkDomainFolder($site->getSubdomain(), $domain->getDomainName());
        }

        $this->certTaskFactory->createCertTaskForAttachment($user, $domain, CertData::ACTION_CREATE);
        $this->logger->info('Created an ACTION_CREATE Cert Task to attach our domain');

        return $site;
    }

    /**
     * @param Domain $domain
     * @param User $user
     * @param Site $previousSite
     * @param Domain $oldDomain
     * @throws AttachDomainException
     * @throws UnknownSiteType
     */
    public function attachFramaDomain(Domain $domain, User $user, Site $previousSite, Domain $oldDomain)
    {
        $newSite = $domain->getSite();

        /**
         * If we're trying to set the domain on a site on which it already was
         */
        if ($newSite === $previousSite) {
            throw new AttachDomainException();
        }

        $createTaskId = $this->certTaskFactory->createCertTaskForAttachment($user, $domain, CertData::ACTION_CREATE);
        $this->logger->info('Sent cert task for new attachment for the domain');

        if ($domain->getStatus() === Domain::DOMAIN_OK && $previousSite !== $newSite) {
            $this->logger->info('Sent cert task for reducting the domain');
            $this->certTaskFactory->createCertTaskForAttachment($user, $oldDomain, CertData::ACTION_REDUCE, CertTask::STATUS_WAITING, $createTaskId);
        }
        $domain->setStatus(Domain::DOMAIN_WAITING_CONFIGURATION);

        $this->entityManager->persist($domain);
        $this->entityManager->flush();
    }
}
