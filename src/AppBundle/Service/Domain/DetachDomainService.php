<?php

namespace AppBundle\Service\Domain;

use AppBundle\Async\AsyncAccountDoku;
use AppBundle\Entity\Certificate\CertData;
use AppBundle\Entity\Domain;
use AppBundle\Entity\User;
use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Exception\APIException;
use AppBundle\Exception\APIException\Release\ReleaseContactException;
use AppBundle\Helper\CertTaskFactory;
use AppBundle\Helper\DomainFactory;
use AppBundle\Service\BaseService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class DetachDomainService extends BaseService
{

    /**
     * @var DomainFactory
     */
    private $domainFactory;

    /**
     * @var CertTaskFactory
     */
    private $certTaskFactory;

    /**
     * @var AsyncAccountDoku
     */
    private $accountDoku;

    /**
     * DetachDomainService constructor.
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     * @param DomainFactory $domainFactory
     * @param CertTaskFactory $certTaskFactory
     * @param AsyncAccountDoku $accountDoku
     */
    public function __construct(
        EntityManagerInterface $entityManager,
                                LoggerInterface $logger,
                                DomainFactory $domainFactory,
                                CertTaskFactory $certTaskFactory,
                                AsyncAccountDoku $accountDoku
    ) {
        parent::__construct($logger, $entityManager);
        $this->domainFactory = $domainFactory;
        $this->certTaskFactory = $certTaskFactory;
        $this->accountDoku = $accountDoku;
    }

    public function detachDomain(Domain $domain, User $user)
    {
        if ($domain->isRegisteredByFrama()) {
            /**
             * If the domain has been registered with us, we have to release the domain
             */
            try {
                $this->logger->info("We registered the domain, so now we release it");
                $this->domainFactory->releaseDomain($domain);
                $this->logger->info("Domain has been released");
            } catch (APIException $e) {
                throw new APIException\Release\ReleaseDomainException($e->getMessage());
            }

            /**
             * We also need to release the contact since there's no way for the user to get it's domain otherwise.
             * However, if the user already has released a domain, it's account is already released too
             *
             * TODO: Find out what happens if the user added it's own BuyDomains Account
             */
            $user = $gandiId = $domain->getUser();
            if ($gandiId = $user->getGandiId()) {
                try {
                    $this->logger->info("We registered the contact, so now we release it");
                    $this->domainFactory->releaseContact($domain);
                    $this->logger->info("Contact has been released");

                    /**
                     * Probably not needed
                     *
                     * $user->removeGandiId();
                     * $em->persist($user);
                     * $em->flush();
                     */
                } catch (APIException $e) {
                    throw new ReleaseContactException($e->getMessage(), $gandiId);
                }
            }
        }

        /**
         * If the site pointed by the domain has other domains, just reduce, don't delete
         */
        if (null !== $domain->getSite()) {
            $site = $domain->getSite();
            if ($site->getDomains()->count() > 1) {
                $this->logger->info("There are multiple domains on this site so we must send an ACTION_REDUCE Cert task");
                $this->certTaskFactory->createCertTaskForAttachment($user, $domain, CertData::ACTION_REDUCE);
            } else {
                /**
                 * Create task to remove the cert & the config file
                 */
                $this->logger->info("The domain is attached to only one site, so we must just send an ACTION_DELETE Cert task");
                $this->certTaskFactory->createCertTaskForAttachment($user, $domain, CertData::ACTION_DELETE);
            }

            /**
             * If the site is a wiki, we need to remove it's symlink
             */
            if ($site instanceof Wiki) {
                $this->logger->info("The domain is attached to a wiki so we need to unlink the specific domain folder");
                $this->accountDoku->setSite($site)->setUser($user)->setLogger($this->logger);
                $this->accountDoku->unlinkDomainFolder($domain->getDomainName());
            }

            $domain->removeSite();
            $this->logger->info("The domain is no longer attached to a website");
        }

        /**
         * Remove it from our database
         */
        $this->entityManager->remove($domain);
        $this->entityManager->flush();
        $this->logger->info("The domain has been deleted");
    }

    /**
     * @param Domain $domain
     * @param User $user
     * @return Domain
     * @throws \Exception
     */
    public function dissociateDomain(Domain $domain, User $user): Domain
    {
        /**
         * If the site pointed by the domain has other domains, just reduce, don't delete
         */
        if ($domain->getSite()->getDomains()->count() > 1) {
            $this->logger->info("There are multiple domains on this site so we must send an ACTION_REDUCE Cert task");
            $this->certTaskFactory->createCertTaskForAttachment($user, $domain, CertData::ACTION_REDUCE);
        } else {
            /**
             * Create task to remove the cert & the config file
             */
            $this->logger->info("The domain is attached to only one site, so we must just send an ACTION_DELETE Cert task");
            $this->certTaskFactory->createCertTaskForAttachment($user, $domain, CertData::ACTION_DELETE);
        }

        $site = $domain->getSite();

        /**
         * If the site is a wiki, we need to remove it's symlink
         */
        if ($site instanceof Wiki) {
            $this->logger->info("The domain is attached to a wiki so we need to unlink the specific domain folder");
            $this->accountDoku->setSite($site)->setUser($user)->setLogger($this->logger);
            $this->accountDoku->unlinkDomainFolder($domain->getDomainName());
        }

        $domain->removeSite();
        $domain->setStatus(Domain::DOMAIN_DETACHED);
        $this->entityManager->persist($domain);
        $this->entityManager->flush();
        $this->logger->info("The domain is no longer attached to a website");

        return $domain;
    }
}
