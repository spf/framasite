<?php

namespace AppBundle\Exception\SiteException;

use Exception;
use Throwable;

class SubDomainFolderExistsException extends Exception
{

    /** @var string */
    private $folder;

    public function __construct(string $folder, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->folder = $folder;
    }

    /**
     * @return string
     */
    public function getFolder(): string
    {
        return $this->folder;
    }
}
