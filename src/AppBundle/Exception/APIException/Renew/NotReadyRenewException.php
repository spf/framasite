<?php

namespace AppBundle\Exception\APIException\Renew;

use AppBundle\Exception\APIException\RenewException;

class NotReadyRenewException extends RenewException
{

    /**
     * @var string
     */
    private $renewDate;

    /**
     * NotReadyRenewException constructor.
     * @param string $message
     * @param string $renewDate
     */
    public function __construct(string $message, string $renewDate)
    {
        parent::__construct($message);
        $this->renewDate = $renewDate;
    }

    /**
     * @return string
     */
    public function getRenewDate(): string
    {
        return $this->renewDate;
    }

    /**
     * @param string $renewDate
     * @return NotReadyRenewException
     */
    public function setRenewDate(string $renewDate): NotReadyRenewException
    {
        $this->renewDate = $renewDate;
        return $this;
    }
}
