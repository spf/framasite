<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Domain;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDomainData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $domain1 = new Domain();
        $domain1
            ->setDomainName('lablon.de')
            ->setExpiresAt(new \DateTime())
            ->setRegisteredAt((new \DateTime())->add(new \DateInterval('P1Y')))
            ->setRegisteredByFrama(true)
            ->setUser($this->getReference('admin-user'))
        ;

        $manager->persist($domain1);

        $this->addReference('foo-domain', $domain1);

        $domain2 = new Domain();
        $domain2->setDomainName('toto.com')
            ->setRegisteredByFrama(false)
        ;

        $manager->persist($domain2);

        $this->addReference('bar-domain', $domain2);

        $domain3 = new Domain();
        $domain3->setDomainName('tutu.com')
            ->setRegisteredByFrama(true);

        $manager->persist($domain3);

        $this->addReference('baz-domain', $domain3);

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 25;
    }
}
