<?php

namespace AppBundle\Form\Blog;

use AppBundle\Entity\Blog\Blog;
use AppBundle\Form\SiteType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogType extends SiteType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('siteName', TextType::class, [
            'label' => 'site.new.informations.site_name.label',
            'attr' => ['placeholder' => 'site.new.informations.site_name.placeholder'],
            'label_attr' => ['class' => 'col-sm-2'],
        ])
        ->add('siteDescription', TextareaType::class, [
            'label' => 'site.new.informations.site_description.label',
            'attr' => ['placeholder' => 'site.new.informations.site_description.placeholder'],
            'label_attr' => ['class' => 'col-sm-2'],
        ])
        ->add('siteKeywords', TextType::class, [
            'label' => 'site.new.informations.site_keywords.label',
            'attr' => ['placeholder' => 'site.new.informations.site_keywords.placeholder'],
            'label_attr' => ['class' => 'col-sm-2'],
        ])
        ->add('siteModules', ChoiceType::class, [
            'label' => 'site.new.informations.site_modules.label',
            'label_attr' => ['class' => 'col-sm-2 checkbox-inline'],
            'expanded' => true,
            'multiple' => true,
            'choices' => [
                'site.module.blog' => Blog::MODULE_BLOG,
                'site.module.cv' => Blog::MODULE_CV,
                'site.module.one_page' => Blog::MODULE_SINGLE_PAGE,
                'site.module.one_page_carrousel' => Blog::MODULE_SINGLE_PAGE_CARROUSEL,
            ]
        ])
        ->add('homepage', ChoiceType::class, [
            'label' => 'site.new.informations.site_homepage.label',
            'label_attr' => ['class' => 'col-sm-2'],
            'expanded' => false,
            'multiple' => false,
            'choices' => [
                'site.module.blog' => Blog::MODULE_BLOG,
                'site.module.cv' => Blog::MODULE_CV,
                'site.module.one_page' => Blog::MODULE_SINGLE_PAGE,
                'site.module.one_page_carrousel' => Blog::MODULE_SINGLE_PAGE_CARROUSEL,
            ],
        ])
        ->add('siteUsers', BlogUserType::class, [
            'lang_choices' => $options['lang_choices'],
            'user' => $options['user'],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Blog::class,
            'lang_choices' => null,
            'user' => null,
        ]);
    }
}
