<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GetDomainType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('domainName', TextType::class, [
                'required' => true,
                'label_attr' => ['class' => 'col-sm-2'],
                'label' => 'domain.name.label',
                'attr' => ['pattern' => '^[a-zA-Z0-9][-a-zA-Z0-9\.]{0,38}[a-zA-Z0-9]$'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Domain',
        ]);
    }
}
