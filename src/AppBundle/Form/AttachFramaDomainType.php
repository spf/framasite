<?php

namespace AppBundle\Form;

use AppBundle\Entity\Site;
use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AttachFramaDomainType extends AbstractType
{
    /** @var User */
    private $user;


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $options['user'];
        $builder
            ->add('site', EntityType::class, [
                'class' => Site::class,
                'choices' => $this->user->getSites(),
                'label' => 'domain.attach.site',
                'label_attr' => ['class' => 'col-sm-3'],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'domain.attach.save',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Domain',
            'validation_groups' => false,
            'user' => null,
        ]);
    }
}
