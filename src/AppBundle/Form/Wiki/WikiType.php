<?php

namespace AppBundle\Form\Wiki;

use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Form\SiteType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class WikiType
 * @package AppBundle\Form\Wiki
 */
class WikiType extends SiteType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('siteName', TextType::class, [
            'label' => 'site.new.informations.site_name.label',
            'label_attr' => ['class' => 'col-sm-2'],
        ])
        ->add('siteDescription', TextareaType::class, [
            'label' => 'site.new.informations.site_description.label',
            'label_attr' => ['class' => 'col-sm-2'],
        ])
        ->add('policy', ChoiceType::class, [
            'choices' => [
                'site.wiki_policy.open' => Wiki::POLICY_OPEN,
                'site.wiki_policy.public' => Wiki::POLICY_PUBLIC,
                'site.wiki_policy.closed' => Wiki::POLICY_CLOSED,
            ],
            'data' => Wiki::POLICY_PUBLIC,
            'label' => 'site.new.informations.policy.label',
            'label_attr' => ['class' => 'col-sm-2'],
        ])
        ->add('siteUsers', WikiUserType::class, [
            'user' => $options['user'],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Wiki::class,
            'user' => null,
        ]);
    }
}
