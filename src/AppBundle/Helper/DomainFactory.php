<?php

namespace AppBundle\Helper;

use AppBundle\Controller\BuyDomains\DomainCommandController;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Domain;
use AppBundle\Entity\Zone\Zone;
use AppBundle\Exception\APIException;
use AppBundle\Exception\LimitationException;
use Narno\Gandi\Api as GandiAPI;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class DomainFactory
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ZoneFactory
     */
    private $zoneFactory;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var GandiAPI
     */
    private $api;

    /**
     * DomainFactory constructor.
     * @param LoggerInterface $logger
     * @param ZoneFactory $zoneFactory
     * @param $apiMode
     * @param $apiKeyProd
     * @param $apiKeyTest
     */
    public function __construct(LoggerInterface $logger, ZoneFactory $zoneFactory, $apiMode, $apiKeyProd, $apiKeyTest)
    {
        $this->logger = $logger;
        $this->zoneFactory = $zoneFactory;
        $this->logger->debug("Connecting to BuyDomains API in " . $apiMode !== 'prod' ? 'test' : 'prod' . "mode");
        $this->api = new GandiAPI($apiMode !== 'prod');
        // set API key
        $this->apiKey = $apiKeyTest;
        if ($apiMode === 'prod') {
            $this->apiKey = $apiKeyProd;
        }
    }

    /**
     * Register a domain (and clone default zone)
     *
     * @param Domain $domain
     * @return mixed
     * @throws \Exception
     */
    public function registerDomain(Domain $domain)
    {
        $this->logger->debug('Registering domain inside domain factory');
        $gandiContacts = [];
        $ownerInfo = $domain->getContact('owner');
        if (is_array($ownerInfo)) { // TODO : check if this work around is still necessary
            $owner = new Contact();
            $owner->setGandiId($ownerInfo['gandiId']);
            $owner->setContactType(Contact::CONTACT_TYPE_OWNER);
            $domain->addContact('owner', $owner);
        }
        foreach ($domain->getContacts() as $key => $contact) {
            /** @var $contact Contact */
            $gandiContacts[$key] = $contact->getGandiId();
        }
        $this->checkDomainForContact($domain, $domain->getContact('owner'));
        $this->logger->info('Gandi Contacts for domain set');


        try {
            /**
             * Clone the default zone so that we don't have to set it afterwards
             */
            $zone = $this->zoneFactory->cloneZone(
                new Zone($this->zoneFactory->getDefaultZone()),
                0,
                '__fsite_' . $domain->getDomainName()
            );
            $this->logger->info('Cloned zone for new domain');

            $params = [
                'admin' => $gandiContacts['admin'],
                'bill' => $gandiContacts['bill'],
                'owner' => $gandiContacts['owner'],
                'tech' => $gandiContacts['tech'],
                'duration' => 1,
                'zone_id' => $zone->getId(),
            ];
        } catch (\Exception $e) {
            $this->logger->error('Issue while cloning zone for domain : ' . $e->getMessage(), [$domain->getDomainName()]);
            throw new APIException("Domain Issue:" . $e->getMessage());
        }

        try {
            $result = $this->api->domain->create(
                [
                    $this->apiKey,
                    $domain->getDomainName(),
                    $params,
                ]
            );
            $this->logger->info('The domain ' . $domain->getDomainName() . ' was created.', [$result]);

            return $result;
        } catch (\Exception $e) {
            $this->logger->error('Issue while registering domain : ' . $e->getMessage(), [$params]);
            throw new APIException("Domain Issue:" . $e->getMessage());
        }
    }

    /**
     * Create contact for a domain
     *
     * @param Contact $contact
     * @param Domain $domain
     * @return mixed
     * @throws \Exception
     */
    public function createContact(Contact $contact, Domain $domain)
    {
        try {
            $contactInfo = [
                'email' => $contact->getEmail(),
                'password' => $contact->getPassword(),
                'given' => $contact->getGivenName(),
                'family' => $contact->getFamilyName(),
                'streetaddr' => $contact->getStreetAddress(),
                'city' => $contact->getCity(),
                'zip' => $contact->getZipCode(),
                'country' => $contact->getCountry(),
                'phone' => $contact->getPhone(),
                'type' => $contact->getType(),
                'data_obfuscated' => $contact->isObfuscateInfo(),
                'mail_obfuscated' => $contact->isObfuscateInfo(),
                'lang' => $contact->getLang(),
            ];

            if ($contact->getType() !== Contact::TYPE_PRIVATE) {
                $contactInfo['orgname'] = $contact->getOrgName();
            }

            /**
             * We need to add extra parameters depending on the domain
             */
            $extension = $domain->getExtension();
            if ($extension && in_array($extension, DomainCommandController::OFFERED_EXTENSIONS)) {
                switch ($extension) {
                    case 'it':
                        if ($contact->getCountry() === 'IT') {
                            switch ($contact->getType()) {
                                case Contact::TYPE_PRIVATE:
                                    $xItRegistrantEntityType = 1;
                                    break;
                                case Contact::TYPE_COMPANY:
                                    $xItRegistrantEntityType = 2;
                                    break;
                                case Contact::TYPE_ASSOCIATION:
                                    $xItRegistrantEntityType = 4;
                                    break;
                                case Contact::TYPE_PUBLIC_BODY:
                                    $xItRegistrantEntityType = 5;
                                    break;
                                default:
                                    $xItRegistrantEntityType = null;
                                    break;
                            }
                        } else {
                            $xItRegistrantEntityType = 7;
                        }
                        if ($xItRegistrantEntityType) {
                            $contactInfo['extra_parameters']['x-it_registrant_entity_type'] = $xItRegistrantEntityType;
                        }
                        break;
                    case 'co.uk':
                        /*switch ($contact->getType()) {
                            case Contact::TYPE_PRIVATE:
                                $xUKContactType = $contact->getCountry() === 'GB' ? 'IND' : 'FIND';
                                break;
                            case Contact::TYPE_COMPANY:
                                $xUKContactType = $contact->getCountry() === 'GB' ? 'LTD' : 'FCORP';
                                break;
                            case Contact::TYPE_ASSOCIATION:
                                $xUKContactType = $contact->getCountry() === 'GB' ? 'PTNR' : 'FOTHER';
                                break;
                            case Contact::TYPE_PUBLIC_BODY:
                                $xUKContactType = $contact->getCountry() === 'GB' ? 'GOV' : 'FOTHER';
                                break;
                            default:
                                $xUKContactType = null;
                                break;
                        }
                        if ($xUKContactType) {
                            $contactInfo['extra_parameters']['x-uk_contact_type'] = $xUKContactType;
                        }*/
                        break;
                    case 'es':
                        if ($contact->getCountry() === 'ES') {
                            switch ($contact->getType()) {
                                case Contact::TYPE_PRIVATE:
                                    $xEKLegalForm = 1;
                                    break;
                                case Contact::TYPE_COMPANY:
                                    $xEKLegalForm = 612;
                                    break;
                                case Contact::TYPE_ASSOCIATION:
                                    $xEKLegalForm = 47;
                                    break;
                                case Contact::TYPE_PUBLIC_BODY:
                                    $xEKLegalForm = 436;
                                    break;
                                default:
                                    $xEKLegalForm = null;
                            }
                        } else {
                            $xEKLegalForm = 877;
                        }
                        if ($xEKLegalForm) {
                            $contactInfo['extra_parameters']['x-es_owner_legalform'] = $xEKLegalForm;
                        }
                }
            }

            $this->logger->debug('Calling can associate with contact info', [$this->apiKey, $contactInfo, $domain->getDomainName()]);
            $checkContact = $this->api->contact->can_associate(
                [$this->apiKey,
                    $contactInfo,
                    [
                        'domain' => $domain->getDomainName(),
                        'owner' => true,
                    ]
                ]
            );
            if (!$checkContact) {
                $this->logger->error('Check contact for Domain API issue', [$checkContact]);
                throw new APIException("Check contact for Domain API issue");
            }
            $this->logger->debug('Checked that contact can get the domain name successfully !');

            $createdContact = $this->api->contact->create(
                [$this->apiKey,
                    $contactInfo,
                ]
            );
            $this->logger->debug('Created contact: '.$createdContact['handle'] . ' with ID ' . $createdContact['id']);
            $contact->setId($createdContact['id'])->setGandiId($createdContact['handle']);
            return $contact;
        } catch (\Exception $e) {
            throw new APIException("Contact Issue:" . $e->getMessage());
        }
    }

    /**
     * TODO : remove me ?
     *
     * @param Domain $domain
     * @param Contact $contact
     * @return bool
     * @throws APIException
     */
    private function checkDomainForContact(Domain $domain, Contact $contact): bool
    {
        try {
            $checkContact = $this->api->contact->can_associate_domain(
                [$this->apiKey,
                $contact->getGandiId(),
                    [
                        'domain' => $domain->getDomainName(),
                        'admin' => $contact->getContactType() === Contact::CONTACT_TYPE_ADMIN,
                        'tech' => $contact->getContactType() === Contact::CONTACT_TYPE_TECH,
                        'bill' => $contact->getContactType() === Contact::CONTACT_TYPE_BILL,
                        'owner' => $contact->getContactType() === Contact::CONTACT_TYPE_OWNER,
                    ]
                ]
            );
            if (!$checkContact) {
                throw new APIException("Contact Issue:" . $checkContact);
            }
            $this->logger->debug('Checked contact: '. $contact->getGandiId());
            return true;
        } catch (\Exception $e) {
            throw new APIException("Contact Issue:" . $e->getMessage());
        }
    }

    /**
     * @param string $domainName
     * @param int $duration
     * @param string $type
     * @param string $action
     * @return float
     * @throws APIException
     */
    public function calculatePriceForAction(string $domainName, int $duration = 1, string $type = 'domains', string $action = 'create')
    {
        try {
            $catalog = $this->api->catalog->list([$this->apiKey, [
                'product' => [
                    'description' => $domainName,
                    'type' => $type,
                ],
                'action' => [
                    'name' => $action,
                    'duration' => $duration
                ]
            ]]);
            if (isset($catalog[0]) && isset($catalog[0]['unit_price']) && isset($catalog[0]['unit_price'][0]) && isset($catalog[0]['unit_price'][0]['price'])) {
                return $catalog[0]['unit_price'][0]['price'];
            }
            return null;
        } catch (\Exception $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param string $domainAsked
     * @param array $extensions
     * @param float $marge
     * @param string $username
     * @return array
     * @throws APIException
     * @throws LimitationException
     */
    public function getSuggestedDomains(string $domainAsked, array $extensions, float $marge, string $username)
    {
        $domainAsked = strtolower($domainAsked);

        try {
            $this->logger->debug('User ' . $username . ' asked for domain ' . $domainAsked);

            $domain = explode('.', $domainAsked, 2);
            if ($domain[0] === $domainAsked) {
                $domainsToCheck = [];
                foreach ($extensions as $extension) {
                    $domainsToCheck[$domain[0] . '.' . $extension] = false;
                }
                $this->logger->debug("User asked for domain without specific extension. List of domains to try", [$domainsToCheck]);
            } else {
                // This part is the real domain
                $domainPart = $domain[0];
                // This is an extension
                $extension = end($domain);
                if (!in_array($extension, $extensions)) {
                    throw new LimitationException("Framasoft doesn't offer this extension");
                }
                $this->logger->debug("Let's try only with extension " . $extension);
                $domainsToCheck[$domainPart . '.' . $extension] = false;
                $this->logger->debug("User asked for domain with specific extension. List of domains to try is only", [$domainsToCheck]);
            }

            $availabilities = [];
            /**
             * FIXME ! This is ugly.
             */
            while (in_array(false, $domainsToCheck)) {
                $this->logger->debug("Calling API to get price of domains", [$domainsToCheck]);
                $availabilitiesAPI = $this->api->domain->price([$this->apiKey, array_keys($domainsToCheck)]);
                $this->logger->debug("API replied with ", [$availabilitiesAPI]);

                foreach ($availabilitiesAPI as $availability) {
                    if ($availability['available'] !== 'pending') {
                        $this->logger->debug($availability['extension'] . " is set !");
                        $availabilities[$availability['extension']] = $availability;
                        unset($domainsToCheck[$availability['extension']]);
                    }
                }
            }

            /**
             * Apply the margin
             */
            $availabilitiesWithMargeApplied = [];
            foreach ($availabilities as $domain) {
                $this->logger->debug('Starting to apply marge to domain ' . $domain['extension']);
                if ($domain['available'] === 'available') {
                    $this->logger->debug('Domain is available');
                    $this->logger->debug('Price before the marge:' . $domain['prices'][0]['unit_price'][0]['price']);
                    $domain['prices'][0]['unit_price'][0]['price'] += ($domain['prices'][0]['unit_price'][0]['price'] * $marge);
                    $this->logger->debug('Price after the marge:' . $domain['prices'][0]['unit_price'][0]['price']);
                }
                $availabilitiesWithMargeApplied[] = $domain;
            }
            return $availabilitiesWithMargeApplied;
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param string $tag
     * @param string $domainName
     * @throws APIException
     */
    public function addTagToDomain(string $tag, string $domainName)
    {
        $this->logger->debug('Adding tag ' . $tag . ' to domain name ' . $domainName);
        try {
            $this->api->domain->tag->add([$this->apiKey, $tag, $domainName]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param Domain $domain
     * @throws APIException
     */
    public function releaseDomain(Domain $domain)
    {
        try {
            $res = $this->api->domain->release([$this->apiKey, $domain->getDomainName()]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
        if ($res !== 1) {
            throw new APIException($res);
        }
    }

    /**
     * @param Domain $domain
     * @return array
     * @throws APIException
     */
    public function renewDomain(Domain $domain)
    {
        $expireYear = $domain->getExpiresAt()->format("Y");
        try {
            return $this->api->domain->renew([$this->apiKey, $domain->getDomainName(), [
                'current_year' => $expireYear,
                'duration' => 1,
            ]]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param Domain $domain
     * @return array
     * @throws APIException
     */
    public function infoDomain(Domain $domain)
    {
        try {
            return $this->api->domain->info([$this->apiKey, $domain->getDomainName()]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param array $filters
     * @return array
     * @throws APIException
     */
    public function getOperationsList(array $filters = [])
    {
        $filters = array_merge($filters, ['>date_created' => '2017-11-23 00:00:00']);
        $params = $filters !== [] ? [$this->apiKey, $filters] : [$this->apiKey];
        try {
            return $this->api->operation->list($params);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param int $operationId
     * @return array
     * @throws APIException
     */
    public function getOperation(int $operationId)
    {
        try {
            return $this->api->operation->info([$this->apiKey, $operationId]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param int $operationId
     * @return array
     * @throws APIException
     */
    public function relaunchOperation(int $operationId)
    {
        try {
            $return = $this->api->operation->relaunch([$this->apiKey, $operationId]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
        if ($return !== 1) {
            throw new APIException("Didn't manage to relaunch operation");
        }
        return $return;
    }

    /**
     * @param string $contactId
     * @throws APIException
     */
    public function releaseContact(string $contactId)
    {
        try {
            $return = $this->api->contact->release([$this->apiKey, $contactId]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
        if ($return !== 1) {
            throw new APIException("Didn't manage to release contact");
        }
    }

    /**
     * @param string|null $contactId
     * @return mixed
     * @throws APIException
     */
    public function infoContact(string $contactId = null)
    {
        try {
            return $this->api->contact->info([$this->apiKey, $contactId]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }
}
