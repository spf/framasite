<?php

namespace AppBundle\Helper;

use AppBundle\Entity\AbstractSiteUser;
use AppBundle\Entity\Blog\Blog;
use AppBundle\Entity\Blog\BlogUser;
use AppBundle\Entity\SinglePage\SinglePage;
use AppBundle\Entity\SinglePage\SinglePageUser;
use AppBundle\Entity\Site;
use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Entity\Wiki\WikiUser;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

class SiteFactory
{

    /**
     * @var string
     */
    private $blogAccountsPath;

    /**
     * @var string
     */
    private $wikiAccountsPath;

    /**
     * @var string
     */
    private $singlePageAccountsPath;

    public function __construct(string $blogAccountsPath, string $wikiAccountsPath, string $singlePageAccountsPath)
    {
        $this->blogAccountsPath = $blogAccountsPath;
        $this->wikiAccountsPath = $wikiAccountsPath;
        $this->singlePageAccountsPath = $singlePageAccountsPath;
    }

    public function loadInformationFromSite(Site $site)
    {
        if ($site instanceof Blog) {
            $file = file_get_contents($this->blogAccountsPath . '/' . $site->getSubdomain() . '/user/config/site.yaml');
            $data = Yaml::parse($file);

            return [
                'name' => $data['title'],
                'description' => $data['metadata']['description'],
                'keywords' => $data['metadata']['keywords'],
                'users' => $this->getUsersForSite($site),
            ];
        } elseif ($site instanceof SinglePage) {
            $file = file_get_contents($this->singlePageAccountsPath . '/' . $site->getSubdomain() . '.frama.site/config.json');
            $data = json_decode($file, true);

            $users = $this->getUsersForSinglePage($data);

            return [
                'name' => $data['siteLabel'],
                'description' => $data['siteDescription'],
                'keywords' => $data['siteKeywords'],
                'users' => $users,
            ];
        } elseif ($site instanceof Wiki) {
            include $this->wikiAccountsPath . '/' . $site->getSubdomain() . '.frama.wiki' . '/conf/local.php';
            return [
                'name' => $conf['title'],
                'description' => $conf['tagline'],
                'keywords' => false,
                'users' => $this->getUsersForSite($site),
            ];
        }
        return null;
    }

    /**
     * @param Site $site
     * @return array
     */
    public function getUsersForSite(Site $site): array
    {
        $finder = new Finder();
        $users = [];

        if ($site instanceof Blog) {
            $finder->files()->in($this->blogAccountsPath . '/' . $site->getSubdomain() . '/user/accounts')->name(
                '*.yaml'
            );

            foreach ($finder as $file) {
                $data = Yaml::parse($file->getContents());
                $user = new BlogUser();
                $user->setUsername($data['fullname'])->setEmail($data['email'])->setAdmin(
                    isset($data['access']['admin'])
                );
                $users[] = $user;
            }
        } elseif ($site instanceof SinglePage) {
            $file = file_get_contents($this->singlePageAccountsPath . '/' . $site->getSubdomain() . '.frama.site/config.json');
            $data = json_decode($file, true);
            $users = $this->getUsersForSinglePage($data);

        } elseif ($site instanceof Wiki) {
            $users = $this->readUserFile($this->wikiAccountsPath . '/' . $site->getSubdomain() . '/conf/users.auth.php');
        }

        return $users;
    }

    /**
     * @param Site $site
     * @param string $userSite
     * @return AbstractSiteUser
     */
    public function getUserForSite(Site $site, string $userSite): AbstractSiteUser
    {
        if ($site instanceof Blog) {
            $user = $this->getUserForBlog($site->getSubdomain(), $userSite);
        } elseif ($site instanceof Wiki) {
            $user = $this->getUserForWiki($site->getSubdomain(), $userSite);
        } elseif ($site instanceof SinglePage) {
            $user = $this->getUserForSinglePage($site->getSubdomain(), $userSite);
        } else {
            $user = null;
        }
        return $user;
    }

    private function getUserForBlog(string $siteDomain, string $userSite): AbstractSiteUser
    {
        $fs = new Filesystem();
        $fs->exists($this->blogAccountsPath . '/' . $siteDomain . '/user/accounts/' . $userSite . '.yaml');

        $data = Yaml::parse(file_get_contents($this->blogAccountsPath . '/' . $siteDomain . '/user/accounts/' . $userSite . '.yaml'));
        $user = new BlogUser();
        $user->setEmail($data['email'])->setAdmin(isset($data['access']['admin']))->setUsername($data['fullname'] ? $data['fullname'] : '');

        return $user;
    }

    private function getUserForSinglePage($siteDomain, $userSite): AbstractSiteUser
    {
        $file = file_get_contents($this->singlePageAccountsPath . '/' . $siteDomain . '.frama.site/config.json');
        $data = json_decode($file, true);
        $user = null;
        foreach ($data['admins'] as $userData) {
            if ($userData['user_id'] == $userSite) {
                $user = new SinglePageUser();
                $user->setUsername($userData['user_id']);
            }
        }
        return $user;
    }

    private function getUsersForSinglePage($data): array
    {
        $users = [];
        foreach ($data['admins'] as $userData) {
            $user = new SinglePageUser();
            $user->setUsername($userData['user_id']);
            $users[] = $user;
        }
        return $users;
    }

    /**
     * Read user data from given file
     *
     * ignores non existing files
     *
     * @param string $file the file to load data from
     * @return array
     */
    private function readUserFile($file)
    {
        $users = [];
        if (!file_exists($file)) {
            return $users;
        }

        $lines = file($file);
        foreach ($lines as $line) {
            $line = preg_replace('/#.*$/', '', $line); //ignore comments
            $line = trim($line);
            if (empty($line)) {
                continue;
            }

            $row = $row = preg_split('/(?<![^\\\\]\\\\)\:/', $line, 5);
            $row = str_replace('\\:', ':', $row);
            $row = str_replace('\\\\', '\\', $row);

            $groups = array_values(array_filter(explode(",", $row[4])));

            $user = new WikiUser();
            $user->setFullName(urldecode($row[2]))->setUsername($row[0])->setEmail($row[3])->setAdmin(in_array('admin', $groups));
            $users[] = $user;
        }
        return $users;
    }
}
