<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Domain;
use AppBundle\Exception\APIException;
use AppBundle\Exception\APIException\Attach\AttachDomainException;
use AppBundle\Exception\APIException\Release\ReleaseContactException;
use AppBundle\Exception\APIException\Release\ReleaseDomainException;
use AppBundle\Exception\DomainException\IncorrectDomainConfigurationException;
use AppBundle\Exception\SiteException\UnknownSiteType;
use AppBundle\Form\AttachDomainType;
use AppBundle\Form\AttachFramaDomainType;
use AppBundle\Form\DetachDomainForm;
use AppBundle\Helper\EmailFactory;
use AppBundle\Service\Domain\AttachDomainService;
use AppBundle\Service\Domain\DetachDomainService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;

class DomainController extends Controller
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var AttachDomainService
     */
    private $attachDomainService;

    /**
     * @var DetachDomainService
     */
    private $detachDomainService;

    public function __construct(
        LoggerInterface $logger,
                                FlashBagInterface $flashBag,
                                TranslatorInterface $translator,
                                AttachDomainService $attachDomainService,
                                DetachDomainService $detachDomainService
    ) {
        $this->logger = $logger;
        $this->flashBag = $flashBag;
        $this->translator = $translator;
        $this->attachDomainService = $attachDomainService;
        $this->detachDomainService = $detachDomainService;
    }

    /**
     * @Route("/domain", name="domain")
     * @return Response
     */
    public function domainIndexAction(): Response
    {
        return $this->render(':default/domain:index.html.twig', [
            'domains' => $this->getUser()->getDomains(),
            'buy_domains' => $this->getParameter('app.framasoft.buy_domains'),
        ]);
    }

    /**
     * @Route("/domain/{domain}", name="domain-view", requirements={"domain": "\d+"})
     *
     * @param Domain $domain
     * @param EmailFactory $emailFactory
     * @return Response
     */
    public function viewDomainAction(Domain $domain, EmailFactory $emailFactory): Response
    {
        $this->checkAuthForDomain($domain);
        $nbMailBoxes = 'N/A';

        /**
         * If we have registered the domain, we can haz mailboxes !
         */
        if ($domain->isRegisteredByFrama()) {
            try {
                $nbMailBoxes = $emailFactory->getNbMailboxes($domain->getDomainName());
            } catch (APIException $e) {
                $this->logger->warning("The domain is registered by us but we didn't manage to find the number of mailboxes : " . $e->getMessage());
                $this->flashBag->add(
                    'warning',
                    $this->translator->trans('flashes.mailbox.count.unable', [], 'messages')
                );
            }
        }
        return $this->render('default/domain/view.html.twig', [
            'domain' => $domain,
            'nbMailBoxes' => $nbMailBoxes,
        ]);
    }

    /**
     * This is when wants to attach his external new domain
     *
     * @Route("/domain/attach", name="attach-domain")
     *
     * @param Request $request
     * @return Response
     * @throws UnknownSiteType
     */
    public function attachDomainAction(Request $request): Response
    {
        if (count($this->getUser()->getSites()) === 0) {
            $this->flashBag->add(
                'warning',
                $this->translator->trans('flashes.domain.attach.no_site', [], 'messages')
            );

            return $this->redirectToRoute('homepage');
        }
        $domain = new Domain();
        $domain->setUser($this->getUser())->setRegisteredByFrama(false)->setRegisteredAt(new \DateTime());

        $attachDomainForm = $this->createForm(AttachDomainType::class, $domain, ['sites' => $this->getUser()->getSites()]);

        $attachDomainForm->handleRequest($request);

        $this->logger->info('Showing attaching domain form by user ' . $this->getUser()->getUsername());

        if ($attachDomainForm->isSubmitted() && $attachDomainForm->isValid()) {
            // create the vhost for the domain

            /** @var AttachDomainService $attachDomainService */
            $site = $this->attachDomainService->attachDomain($domain, $this->getUser());

            $this->flashBag->add(
                'info',
                $this->translator->trans('flashes.domain.attached', ['%domain%' => $domain->getDomainName(), '%site%' => $site->getSubdomain()], 'messages')
            );
            // send a success (or not ?) notification
            return $this->redirectToRoute('homepage');
        }

        $v4 = @dns_get_record('frama.site', DNS_A)[0]['ip'];
        $v6 = @dns_get_record('frama.site', DNS_AAAA)[0]['ipv6'];
        if (!($v4 &  $v6)) {
            $this->flashBag->add('warning', $this->translator->trans('flashes.domain.no_connection', [], 'messages'));
        }

        return $this->render(':default/domain:attach.html.twig', [
            'form' => $attachDomainForm->createView(),
            'ip' => [
                'v4' => $v4,
                'v6' => $v6,
            ]
        ]);
    }

    /**
     * @Route("/domain/attach/check", name="attach-domain-check")
     *
     * @param Request $request
     * @return Response
     */
    public function checkIfDomainIsReadyAction(Request $request): Response
    {
        if (!$domain = $request->get('domain')) {
            return new JsonResponse(['error' => 'Missing domain information'], 400);
        }

        try {
            $this->checkIfDomainIsReady($domain);
            return new JsonResponse(['success' => true]);
        } catch (IncorrectDomainConfigurationException $e) {
            if ($e->getErrorType() === IncorrectDomainConfigurationException::ERROR_TYPE_MISSING) {
                return new JsonResponse(['error' => "No IPv". $e->getIpType() . " associated with this domain"], 400);
            }
            if ($e->getErrorType() === IncorrectDomainConfigurationException::ERROR_TYPE_WRONG) {
                return new JsonResponse(['error' => "DNS Zone ". $e->getIpType() === IncorrectDomainConfigurationException::IP_V4 ? 'A' : 'AAAA' ." doesn't match framasites IPv" . $e->getIpType()], 400);
            }
            return new JsonResponse(['error' => "Unknown error"], 500);
        }
    }


    /**
     * This is when an user wants to attach an existing domain
     *
     * @Route("/domain/attach-frama/{domain}", name="attach-frama-domain", requirements={"domain": "\d+"})
     *
     * @param Request $request
     * @param Domain $domain
     * @return Response
     * @throws \Exception
     */
    public function attachFramaDomainAction(Request $request, Domain $domain): Response
    {
        $this->checkAuthForDomain($domain);
        $attachDomainForm = $this->createForm(AttachFramaDomainType::class, $domain, ['user' => $this->getUser()]);

        $oldDomain = $domain;
        $previousSite = $domain->getSite();
        $attachDomainForm->handleRequest($request);

        if ($attachDomainForm->isSubmitted() && $attachDomainForm->isValid()) {
            try {
                $this->attachDomainService->attachFramaDomain($domain, $this->getUser(), $previousSite, $oldDomain);

                $this->flashBag->add(
                    'info',
                    $this->translator->trans('flashes.domain.attached', ['%domain%' => $domain->getDomainName(), '%site%' => $domain->getSite()->getSubdomain()], 'messages')
                );
            } catch (AttachDomainException $e) {
                $this->flashBag->add(
                    'warning',
                    $this->translator->trans('flashes.domain.attached_same', ['%domain%' => $domain->getDomainName(), '%site%' => $domain->getSite()->getSubdomain()], 'messages')
                );
                $this->logger->warning("User " . $this->getUser()->getUsername() . " wanted to set it's domain " . $domain->getDomainName() . " to the site it had before");
                return $this->redirectToRoute('homepage');
            }

            // send a success (or not ?) notification
            return $this->redirectToRoute('homepage');
        }
        return $this->render('default/domain/attach-frama.html.twig', [
            'form' => $attachDomainForm->createView(),
            'domain' => $domain
        ]);
    }

    /**
     * This will remove the domain in our local database
     *
     * @Route("/domain/{domain}/detach", name="detach-domain", requirements={"domain": "\d+"})
     *
     * @param Request $request
     * @param Domain $domain
     * @return Response
     */
    public function detachDomainAction(Request $request, Domain $domain): Response
    {
        $this->checkAuthForDomain($domain);
        $detachDomainForm = $this->createForm(DetachDomainForm::class);

        $detachDomainForm->handleRequest($request);
        $this->logger->info('Showing detaching domain form ' . $domain->getDomainName() . ' from user ' . $this->getUser()->getUsername());

        if ($detachDomainForm->isSubmitted() && $detachDomainForm->isValid()) {
            try {
                /** @var DetachDomainService $detachService */
                $this->detachDomainService->detachDomain($domain, $this->getUser());
                $this->flashBag->add(
                    'info',
                    $this->translator->trans(
                        'flashes.domain.detached.success',
                        ['%domain%' => $domain->getDomainName()],
                        'messages'
                    )
                );
            } catch (ReleaseDomainException $e) {
                $this->logger->error('When releasing domain ' . $domain->getDomainName() . ' from user ' . $this->getUser()->getUsername() . ', an error occurred : ' . $e->getMessage());
                $this->flashBag->add(
                    'danger',
                    $this->translator->trans(
                        'flashes.domain.detached.error',
                        ['%domain%' => $domain->getDomainName(), '%error%' => $e->getMessage()],
                        'messages'
                    )
                );
            } catch (ReleaseContactException $e) {
                $this->logger->error('When releasing contact ' . $e->getGandiId() . ' from user ' . $this->getUser()->getUsername() . ', an error occurred : ' . $e->getMessage());
                $this->flashBag->add(
                    'danger',
                    $this->translator->trans(
                        'flashes.contact.detached.error',
                        ['%contact%' => $e->getGandiId(), '%error%' => $e->getMessage()],
                        'messages'
                    )
                );
            }

            return $this->redirectToRoute('domain');
        }
        $referer = $request->headers->get('referer') ?: $this->get('router')->generate('homepage', [], Router::ABSOLUTE_URL);

        return $this->render('default/domain/detach.html.twig', [
            'domain' => $domain,
            'form' => $detachDomainForm->createView(),
            'referer' => $referer,
            'contactId' => $this->getUser()->getGandiId(),
        ]);
    }

    /**
     * @Route("/domain/dissociate/{domain}", name="dissociate-domain", requirements={"domain": "\d+"})
     *
     * @param Domain $domain
     * @return Response
     * @throws \Exception
     */
    public function dissociateDomainAction(Domain $domain): Response
    {
        $this->checkAuthForDomain($domain);
        $site = $domain->getSite();

        $this->detachDomainService->dissociateDomain($domain, $this->getUser());

        $this->flashBag->add(
            'info',
            $this->translator->trans('flashes.domain.dissociated', [
                '%domain%' => $domain->getDomainName(),
                '%site%' => $site->getSubdomain(),
            ], 'messages')
        );

        return $this->redirectToRoute('domain');
    }

    /**
     * @param string $domain
     * @return bool
     * @throws IncorrectDomainConfigurationException
     */
    private function checkIfDomainIsReady(string $domain): bool
    {
        $ipv4 = dns_get_record($domain, DNS_A);
        $ipv6 = dns_get_record($domain, DNS_AAAA);

        if (!$ipv4) {
            throw new IncorrectDomainConfigurationException(IncorrectDomainConfigurationException::ERROR_TYPE_MISSING, IncorrectDomainConfigurationException::IP_V4);
        }

        if (!$ipv6) {
            throw new IncorrectDomainConfigurationException(IncorrectDomainConfigurationException::ERROR_TYPE_MISSING, IncorrectDomainConfigurationException::IP_V6);
        }

        if (dns_get_record("frama.site", DNS_A)[0]['ip'] !== $ipv4[0]['ip']) {
            throw new IncorrectDomainConfigurationException(IncorrectDomainConfigurationException::ERROR_TYPE_WRONG, IncorrectDomainConfigurationException::IP_V4);
        }

        if (dns_get_record("frama.site", DNS_AAAA)[0]['ipv6'] !== $ipv6[0]['ipv6']) {
            throw new IncorrectDomainConfigurationException(IncorrectDomainConfigurationException::ERROR_TYPE_WRONG, IncorrectDomainConfigurationException::IP_V6);
        }
        return true;
    }

    /**
     * @param Domain $domain
     * @return bool
     */
    private function checkAuthForDomain(Domain $domain)
    {
        if ($domain->getUser() !== $this->getUser() && !($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('SUPER_ADMIN'))) {
            throw new AccessDeniedException("You don't have access to this domain");
        }
        return true;
    }
}
