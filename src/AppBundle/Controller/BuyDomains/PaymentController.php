<?php

namespace AppBundle\Controller\BuyDomains;

use AppBundle\Entity\Command;
use AppBundle\Entity\Domain;
use AppBundle\Entity\User;
use AppBundle\Helper\PaymentFactory;
use AppBundle\Service\StripeKeyService;
use Psr\Log\LoggerInterface;
use AppBundle\Service\Email\InvoiceService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PaymentController extends Controller
{
    /**
     * @var PaymentFactory
     */
    private $paymentFactory;

    /**
     * @var StripeKeyService
     */
    private $stripeKeyService;

    /**
     * @var InvoiceService
     */
    private $invoiceService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * PaymentController constructor.
     * @param LoggerInterface $logger
     * @param PaymentFactory $paymentFactory
     * @param InvoiceService $invoiceService
     * @param StripeKeyService $stripeKeyService
     */
    public function __construct(LoggerInterface $logger, PaymentFactory $paymentFactory, InvoiceService $invoiceService, StripeKeyService $stripeKeyService)
    {
        $this->logger = $logger;
        $this->paymentFactory = $paymentFactory;
        $this->invoiceService = $invoiceService;
        $this->stripeKeyService = $stripeKeyService;
    }

    /**
     * @Route("/payment/{command}/choice", name="payment-choice")
     *
     * @param Command $command
     * @return Response
     */
    public function paymentChoiceAction(Command $command): Response
    {
        $cards = [];
        $defaultCardId = null;
        if ($stripeUserId = $this->getUser()->getStripeId()) {
            $cards = $this->paymentFactory->listCards($stripeUserId);
            $defaultCardId = $this->paymentFactory->getUser($stripeUserId)->default_source;
        }
        return $this->render('default/payment/choice.html.twig', [
            'command' => $command,
            'stripe_public_key' => $this->stripeKeyService->getPublicKey(),
            'cards' => $cards,
            'default_card_id' => $defaultCardId,
        ]);
    }

    /**
     * @Route("/payment/process/{command}", name="payment-process")
     *
     * @param Request $request
     * @param Command $command
     * @return Response
     */
    public function processPaymentAction(Request $request, Command $command): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($request->request->has('stripeToken')) {
            // we have a stripe token we will need to process a card
            $token = $request->request->get('stripeToken');

            if ($request->request->has('no-save-card') && $request->request->get('no-save-card') === 'on') {
                /**
                 * This is a direct payment without saving the card
                 */
                $this->logger->info("This is a direct payment without saving the card");

                $charge = $this->paymentFactory->charge($command->getTotal() * 100, $token, 'Direct payment for ' . $command->getDomainNames());
                $command->setPaymentType(Command::PAYMENT_TYPE_DIRECT);
            } else {
                /**
                 * This a payment with a new card that will be saved
                 */
                $this->logger->info("This a payment with a new card that will be saved");

                /**
                 * If the user hasn't got any customer account, let's create one
                 */
                if (!$user->getStripeId()) {
                    $this->logger->info("We just create and save the user before saving the card");
                    $customer = $this->paymentFactory->createUser($user->getEmail(), $token);
                    $user->setStripeId($customer->id);

                    $this->getDoctrine()->getManager()->persist($user);
                    $this->getDoctrine()->getManager()->flush();
                }

                $card = $this->paymentFactory->createCard($user->getStripeId(), $token);

                $charge = $this->paymentFactory->chargeUser($command->getTotal() * 100, $user->getStripeId(), 'Payment with a new card for ' . $command->getDomainNames(), $card->id);
                $command->setPaymentType(Command::PAYMENT_TYPE_SAVED_CARD);
            }
        } else {
            /**
             * If we don't have any stripe token
             */
            if ($request->request->has('card')) {
                /**
                 * Either we choose a card
                 */
                $cardId = $request->request->get('card');
                $charge = $this->paymentFactory->chargeUser($command->getTotal() * 100, $user->getStripeId(), 'Payment from a saved card for ' . $command->getDomainNames(), $cardId);
                $command->setPaymentType(Command::PAYMENT_TYPE_SAVED_CARD);
            } else {
                /**
                 * Either we have an issue
                 */
                $this->logger->error("Couldn't get stripe token from the form");
                $command->setStatus(Command::STATUS_PAYMENT_ISSUE);
                return $this->redirectToRoute('payment-cancel');
            }
        }
        $command->setStripeId($charge->id);

        $this->getDoctrine()->getManager()->persist($command);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('payment-success', ['command' => $command->getId()]);
    }

    /**
     * @Route("/payment/success/{command}", name="payment-success")
     * @param Command $command
     * @return Response
     */
    public function successPaymentAction(Command $command): Response
    {
        $this->logger->info("Payment for a command " . $command->getId() . " was a success");

        $this->postSuccessPayment($command);

        return $this->render(
            ':default/payment:success.html.twig',
                             [
                                 'command' => $command,
                             ]
        );
    }

    /**
     * @param Command $command
     * @return Response
     */
    private function postSuccessPayment(Command $command)
    {
        $this->checkAuthForCommand($command);
        $command->setStatus(Command::STATUS_RESPONDED);
        $em = $this->getDoctrine()->getManager();
        $em->persist($command);
        $em->flush();

        $this->logger->info('Payment has been issued for command ' . $command->getId() . ", we can now set domains to payed");

        $domains = $command->getDomains();
        foreach ($domains as $domain) {
            /** @var $domain Domain */
            $domain->setStatus(Domain::DOMAIN_PAYED);
            $em->persist($domain);
        }
        $em->flush();

        return $this->render(
            ':default/payment:success.html.twig',
                             [
                                 'command' => $command,
                             ]
        );
    }

    /**
     * @Route("/payment/cancel/{command}", name="payment-cancel")
     * @param Command $command
     * @return Response
     */
    public function cancelledPaymentAction(Command $command): Response
    {
        $this->checkAuthForCommand($command);

        $this->logger->info('Command ' . $command->getId() . ' has been cancelled');
        $command->setStatus(Command::STATUS_PAYMENT_CANCELLED);
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $user->setActiveCommand();
        $em->persist($user);
        $em->persist($command);
        $em->flush();
        return $this->render('default/payment/cancel.html.twig', ['command' => $command]);
    }

    /**
     * @param Command $command
     * @return bool
     */
    private function checkAuthForCommand(Command $command)
    {
        if ($command->getUser() !== $this->getUser() && !($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('SUPER_ADMIN'))) {
            throw new AccessDeniedException("You don't have access to this command");
        }
        return true;
    }
}
