<?php

namespace AppBundle\Controller\BuyDomains;

use AppBundle\Entity\Domain;
use AppBundle\Entity\Mailbox;
use AppBundle\Exception\APIException;
use AppBundle\Form\MailboxType;
use AppBundle\Helper\EmailFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class MailboxController extends Controller
{

    /**
     * @var EmailFactory
     */
    private $emailFactory;

    /**
     * MailboxController constructor.
     * @param EmailFactory $emailFactory
     */
    public function __construct(EmailFactory $emailFactory)
    {
        $this->emailFactory = $emailFactory;
    }

    /**
     * @Route("/domain/{domain}/email", name="email-index")
     *
     * @param Domain $domain
     * @return Response
     */
    public function indexMailboxAction(Domain $domain): Response
    {
        $this->checkAuthForDomain($domain);
        $mailboxesData = $this->emailFactory->listMailboxes($domain->getDomainName());
        $mailboxes = [];
        foreach ($mailboxesData as $mailboxesDatum) {
            $mailbox = new Mailbox();
            $mailbox->setDomain($domain)->setLogin($mailboxesDatum['login'])->setQuota($mailboxesDatum['quota']);
            $mailboxes[] = $mailbox;
        }
        return $this->render('default/mail/index.html.twig', ['mailboxes' => $mailboxes, 'domain' => $domain]);
    }

    /**
     * @Route("/domain/{domain}/email/view/{login}", name="email-view")
     *
     * @param Domain $domain
     * @param string$login
     * @return Response
     */
    public function viewMailboxAction(Domain $domain, string $login): Response
    {
        $this->checkAuthForDomain($domain);
        $mailboxData = $this->emailFactory->infoMailbox($domain->getDomainName(), $login);
        $mailbox = new Mailbox();
        $mailbox->setDomain($domain)->setLogin($mailboxData['login'])->setQuota($mailboxData['quota'])->setAliases($mailboxData['aliases'])->setFallbackEmail($mailboxData['fallback_email']);
        return $this->render('default/mail/view.html.twig', ['mailbox' => $mailbox]);
    }

    /**
     * @Route("/domain/{domain}/email/new", name="email-new")
     *
     * @param Request $request
     * @param Domain $domain
     * @return Response
     */
    public function createMailboxAction(Request $request, Domain $domain): Response
    {
        $this->checkAuthForDomain($domain);
        $mailbox = new Mailbox();
        $mailbox->setDomain($domain)->setFallbackEmail($this->getUser()->getEmail());
        $newMailboxForm = $this->createForm(MailboxType::class, $mailbox);
        $newMailboxForm->handleRequest($request);

        if ($newMailboxForm->isSubmitted() && $newMailboxForm->isValid()) {
            try {
                $this->get('logger')->info("User " . $this->getUser()->getUsername() . ' tries to create mailbox' . $mailbox->getLogin() . '@' . $mailbox->getDomain());
                $this->emailFactory->createMailbox(
                    $mailbox->getDomain(),
                    $mailbox->getLogin(),
                    $mailbox->getPassword(),
                    $mailbox->getFallbackEmail()
                );
                $this->get('logger')->debug('Mailbox created');
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans('flashes.mailbox.new.success', [], 'messages')
                );
            } catch (APIException $e) {
                $this->get('logger')->error("Mailbox couldn't be created, error is " . $e->getMessage());
                $this->get('session')->getFlashBag()->add(
                    'danger',
                    $this->get('translator')->trans('flashes.mailbox.new.error', ['%error%' => $e->getMessage()], 'messages')
                );
            }
            return $this->redirectToRoute('email-index', ['domain' => $domain->getId()]);
        }
        return $this->render('default/mail/new.html.twig', ['form' => $newMailboxForm->createView(), 'domain' => $domain]);
    }

    /**
     * @Route("/domain/{domain}/email/{login}/delete", name="email-delete")
     *
     * @param Domain $domain
     * @param $login
     * @return Response
     */
    public function deleteMailboxAction(Domain $domain, string $login): Response
    {
        $this->checkAuthForDomain($domain);
        try {
            $this->emailFactory->deleteMailbox($domain->getDomainName(), $login);
            $this->get('logger')->info('Mailbox ' . $login . '@' . $domain->getDomainName() . ' has been deleted');

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('flashes.mailbox.deleted.success', [], 'messages')
            );
        } catch (APIException $e) {
            $this->get('logger')->error('Error while deleting mailbox ' . $login . '@' . $domain->getDomainName() . ': ' . $e->getMessage());
            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('flashes.mailbox.deleted.error', ['%error%' => $e->getMessage()], 'messages')
            );
        }
        return $this->redirectToRoute('email-index', ['domain' => $domain->getId()]);
    }

    /**
     * @param Domain $domain
     * @return bool
     */
    private function checkAuthForDomain(Domain $domain)
    {
        if ($domain->getUser() !== $this->getUser() && !($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('SUPER_ADMIN'))) {
            throw new AccessDeniedException("You don't have access to this domain");
        }
        return true;
    }
}
