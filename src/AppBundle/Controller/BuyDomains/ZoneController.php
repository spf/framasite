<?php

namespace AppBundle\Controller\BuyDomains;

use AppBundle\Entity\Domain;
use AppBundle\Entity\Zone\Record;
use AppBundle\Entity\Zone\Version;
use AppBundle\Entity\Zone\Zone;
use AppBundle\Exception\APIException;
use AppBundle\Form\Zone\RecordType;
use AppBundle\Form\Zone\ZoneType;
use AppBundle\Helper\ZoneFactory;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ZoneController extends Controller
{

    /**
     * @var ZoneFactory
     */
    private $zoneFactory;

    /**
     * ZoneController constructor.
     * @param ZoneFactory $zoneFactory
     */
    public function __construct(ZoneFactory $zoneFactory)
    {
        $this->zoneFactory = $zoneFactory;
    }

    /**
     * @Route("/zone/{zone}", name="info-zone", requirements={"zone": "\d+"})
     *
     * @param int $zone
     * @return Response
     */
    public function viewDNSZoneAction(int $zone): Response
    {
        $this->checkAuthForZone($zone);
        $zone = $this->zoneFactory->getZoneInfo($zone);
        return $this->render('default/admin/zone/view.html.twig', [
            'zone' => $zone,
        ]);
    }

    /**
     * @Route("/zone/create", name="create-zone")
     *
     * @param Request $request
     * @return Response
     */
    public function newDNSZoneAction(Request $request): Response
    {
        $zone = new Zone();
        $zoneForm = $this->createForm(ZoneType::class, $zone);
        $zoneForm->handleRequest($request);

        if ($zoneForm->isSubmitted() && $zoneForm->isValid()) {
            $zone = $this->zoneFactory->createZone($zone);

            $this->get('logger')->info($this->getUser()->getUsername() . ' created a new DNS zone');
            $this->get('session')->getFlashBag()->add(
                'info',
                $this->get('translator')->trans('flashes.admin.zone.created', ['%zoneName%' => $zone->getName()], 'messages')
            );

            return $this->redirectToRoute('admin-info-zone', ['zone' => $zone->getId()]);
        }
        return $this->render('default/admin/zone/new.html.twig', [
            'form' => $zoneForm->createView(),
        ]);
    }

    /**
     * @Route("/zone/{zone}/create", name="create-version")
     *
     * @param int $zone
     * @return Response
     */
    public function newDNSZoneVersionAction(int $zone): Response
    {
        $version = new Version(new Zone($zone));

        $version = $this->zoneFactory->createVersion($version);

        $this->get('logger')->info($this->getUser()->getUsername() . ' created a new version for the DNS zone ' . $zone);
        $this->get('session')->getFlashBag()->add(
            'info',
            $this->get('translator')->trans('flashes.admin.zone.version.created', ['%version%' => $version->getId()], 'messages')
        );
        return $this->redirectToRoute('admin-info-zone', ['zone' => $zone]);
    }

    /**
     * @Route("/zone/{zone}/{version}", name="view-version")
     *
     * @param int $zone
     * @param int $version
     * @return Response
     */
    public function viewDNSZoneVersionAction(int $zone, int $version): Response
    {
        $zoneObj = $this->zoneFactory->getZoneInfo($zone);
        $versionObj = new Version($zoneObj);
        $versionObj->setId($version);
        $version = $this->zoneFactory->getZoneVersionRecords($versionObj);
        return $this->render('default/admin/zone/version/view.html.twig', [
            'version' => $version,
        ]);
    }

    /**
     * @Route("/zone/{zone}/{version}/delete", name="delete-version")
     *
     * @param int $zone
     * @param int $version
     * @return Response
     */
    public function deleteDNSZoneVersionAction(int $zone, int $version): Response
    {
        $zoneObj = $this->zoneFactory->getZoneInfo($zone);
        $nbZones = $this->zoneFactory->countZoneVersions($zoneObj);

        /**
         * We need at least two zones
         */
        if ($nbZones < 2) {
            $this->get('session')->getFlashBag()->add(
                'info',
                $this->get('translator')->trans('flashes.admin.zone.version.delete.too_low', [], 'messages')
            );
            return $this->redirectToRoute('info-zone', ['zone' => $zone]);
        }

        /**
         * We can't delete current version
         */
        if ($zoneObj->getCurrentVersion() === $version) {
            $this->get('session')->getFlashBag()->add(
                'info',
                $this->get('translator')->trans('flashes.admin.zone.version.delete.current_version', [], 'messages')
            );
            return $this->redirectToRoute('info-zone', ['zone' => $zone]);
        }

        $versionObj = new Version($zoneObj);
        $versionObj->setId($version);
        $this->zoneFactory->deleteZoneVersion($versionObj);
        $this->get('logger')->info($this->getUser()->getUsername() . ' deleted the DNS zone version ' . $version . ' for the zone ' . $zone);
        return $this->redirectToRoute('admin-info-zone', ['zone' => $zone]);
    }

    /**
     * @Route("/zone/{zone}/{version}/new", name="add-record")
     *
     * @param Request $request
     * @param int $zone
     * @param int $version
     * @return Response
     */
    public function addDNSRecordAction(Request $request, int $zone, int $version): Response
    {
        $zoneObj = new Zone($zone);
        $versionObj = new Version($zoneObj);
        $versionObj->setId($version);
        $record = new Record();
        $zoneForm = $this->createForm(RecordType::class, $record);
        $zoneForm->handleRequest($request);

        if ($zoneForm->isSubmitted() && $zoneForm->isValid()) {
            $this->zoneFactory->addRecord($record, $versionObj);

            $this->get('logger')->info($this->getUser()->getUsername() . ' added a record to the DNS zone version ' . $version . ' for the zone ' . $zone);
            $this->get('session')->getFlashBag()->add(
                'info',
                $this->get('translator')->trans('flashes.admin.zone.record.added', [], 'messages')
            );

            return $this->redirectToRoute('admin-view-version', ['zone' => $zoneObj->getId(), 'version' => $versionObj->getId()]);
        }
        return $this->render('default/admin/zone/record/new.html.twig', [
            'form' => $zoneForm->createView(),
        ]);
    }

    /**
     * @Route("/zone/{zone}/{version}/active", name="set-zone-version")
     *
     * @param int $zone
     * @param int $version
     * @return Response
     */
    public function setZoneVersionActive(int $zone, int $version): Response
    {
        $versionObj = new Version(new Zone($zone));
        $versionObj->setId($version);
        try {
            $this->zoneFactory->setZoneVersionActive($versionObj);
            $this->get('logger')->info($this->getUser()->getUsername() . ' set the DNS zone version ' . $version . ' active for the zone ' . $zone);
            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('flashes.admin.zone.version.set_active.success', [], 'messages')
            );
        } catch (APIException $e) {
            $this->get('logger')->error($this->getUser()->getUsername() . ' set the DNS zone version ' . $version . ' active for the zone ' . $zone . ', but there was an issue : ' . $e->getMessage());
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans('flashes.admin.zone.version.set_active.error', [], 'messages')
            );
        }
        return $this->redirectToRoute('admin-info-zone', ['zone' => $zone]);
    }


    /**
     * @param $zoneId
     *
     * We can't get user information from zone so we need to check every domain the user has to check if the zone matches
     */
    private function checkAuthForZone($zoneId)
    {
        /** @var ArrayCollection $domains */
        $domains = $this->getUser()->getDomains()->filter(function ($elem) {
            /** @var $elem Domain */
            return $elem->isRegisteredByFrama() === true;
        });
        $hasRights = false;
        $i = 0;
        $this->get('logger')->debug('Nb domains to test '. $domains->count());
        while ($i <= $domains->count() && !$hasRights) {
            $this->get('logger')->debug('testing domain '. (string) $domains->get($i));
            $hasRights = isset($domains[$i]) && null !== $domains[$i]->getDnsZone() && $domains[$i]->getDnsZone() === $zoneId;
            $i++;
        }
        if (false === $hasRights) {
            throw new AccessDeniedException("You don't have access to this zone");
        }
    }
}
