<?php

namespace AppBundle\Entity\Wiki;

use AppBundle\Entity\AbstractSiteUser;
use Symfony\Component\Validator\Constraints as Assert;

class WikiUser extends AbstractSiteUser
{
    /**
     * WikiUser constructor.
     * @param Wiki|null $site
     */
    public function __construct(Wiki $site = null)
    {
        parent::__construct($site);
        $this->admin = true;
    }

    /**
     * @var string
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @var bool
     */
    protected $admin = true;

    /**
     * @var string
     */
    protected $fullName;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return WikiUser
     */
    public function setEmail($email): WikiUser
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->admin;
    }

    /**
     * @param bool $admin
     * @return WikiUser
     */
    public function setAdmin(bool $admin): WikiUser
    {
        $this->admin = $admin;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     * @return WikiUser
     */
    public function setFullName(string $fullName): WikiUser
    {
        $this->fullName = $fullName;
        return $this;
    }
}
