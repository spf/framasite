<?php

namespace AppBundle\Entity\SinglePage;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Site;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class SinglePage extends Site
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    protected $siteName;

    /**
     * @var string
     */
    protected $siteDescription;

    /**
     * @var string
     */
    protected $siteKeywords;

    /**
     * @return string
     */
    public function getSiteName()
    {
        return $this->siteName;
    }

    /**
     * @param string $siteName
     * @return SinglePage
     */
    public function setSiteName(string $siteName): SinglePage
    {
        $this->siteName = $siteName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSiteDescription()
    {
        return $this->siteDescription;
    }

    /**
     * @param string $siteDescription
     * @return SinglePage
     */
    public function setSiteDescription(string $siteDescription): SinglePage
    {
        $this->siteDescription = $siteDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getSiteKeywords()
    {
        return $this->siteKeywords;
    }

    /**
     * @param string $siteKeywords
     * @return SinglePage
     */
    public function setSiteKeywords(string $siteKeywords): SinglePage
    {
        $this->siteKeywords = $siteKeywords;
        return $this;
    }

    public function __toString()
    {
        return $this->subdomain . '.frama.site';
    }
}
