<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as FramasitesAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DomainRepository")
 * @ORM\Table(name="domain")
 * @ORM\HasLifecycleCallbacks()
 * @FramasitesAssert\ConstraintValidDomainConfiguration
 * @UniqueEntity(
 *     fields={"domainName"},
 *     message="domain.attach.already_attached"
 * )
 */
class Domain
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"domain_register"})
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\NotEqualTo("frama.site")
     * @Assert\NotEqualTo("frama.wiki")
     * @ORM\Column(name="domain_name", type="string", unique=true)
     * @Groups({"domain_register"})
     */
    private $domainName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="registered_by_frama", type="boolean")
     * @Groups({"domain_register"})
     */
    private $registeredByFrama;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registered_at", type="datetime", nullable=true)
     */
    private $registeredAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires_at", type="datetime", nullable=true)
     */
    private $expiresAt;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     * @Groups({"domain_register"})
     */
    private $status;

    /**
     * @var Site
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site", inversedBy="domains")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id")
     */
    private $site;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="domains")
     * @Groups({"domain_register"})
     */
    private $user;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CommandDomain", mappedBy="domain")
     */
    protected $commands;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $dnsZone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $lastGandiOperation;

    /**
     * @var array
     * @Serializer\Type("array<string,AppBundle\Entity\Contact>")
     * @Groups({"domain_register"})
     */
    private $contacts;

    /** Statuses */
    const DOMAIN_UNKNOWN_ERROR = 0;
    const DOMAIN_REQUESTED = 10;
    const DOMAIN_PAYED = 20;
    const DOMAIN_VALIDATED = 30;
    const DOMAIN_GANDI_CREATED = 31;
    const DOMAIN_GANDI_ERROR = 32;
    const DOMAIN_DNS_CHANGED = 40;
    const DOMAIN_WAITING_CONFIGURATION = 50;
    const DOMAIN_VHOST_ADDED = 60; // Depreciated
    const DOMAIN_OK = 100;
    const DOMAIN_EXPIRED = 70;
    const DOMAIN_DETACHED = 150;
    const DOMAIN_REMOVED = 200;  // Depreciated

    /**
     * Domain constructor.
     */
    public function __construct()
    {
        $this->contacts = [];
        $this->status = self::DOMAIN_REQUESTED;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Domain
     */
    public function setId(int $id): Domain
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * @param string $domainName
     * @return Domain
     */
    public function setDomainName(string $domainName): Domain
    {
        $this->domainName = $domainName;
        return $this;
    }

    /**
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param Site $site
     * @return Domain
     */
    public function setSite(Site $site): Domain
    {
        $this->site = $site;
        $this->site->addDomain($this);
        return $this;
    }

    /**
     * @return Domain
     */
    public function removeSite(): Domain
    {
        $this->site = null;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt()
    {
        return $this->registeredAt;
    }

    /**
     * @param \DateTime $registeredAt
     * @return Domain
     */
    public function setRegisteredAt(\DateTime $registeredAt): Domain
    {
        $this->registeredAt = $registeredAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Domain
     */
    public function setStatus(int $status): Domain
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @param \DateTime $expiresAt
     * @return Domain
     */
    public function setExpiresAt(\DateTime $expiresAt): Domain
    {
        $this->expiresAt = $expiresAt;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRegisteredByFrama(): bool
    {
        return $this->registeredByFrama;
    }

    /**
     * @param bool $registeredByFrama
     * @return Domain
     */
    public function setRegisteredByFrama(bool $registeredByFrama = true): Domain
    {
        $this->registeredByFrama = $registeredByFrama;
        return $this;
    }

    /**
     * @return array
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param array $contacts
     */
    public function setContacts(array $contacts)
    {
        $this->contacts = $contacts;
    }

    /**
     * @param string $key
     * @param Contact $contact
     * @return Domain
     */
    public function addContact(string $key, Contact $contact): Domain
    {
        $this->contacts[$key] = $contact;
        return $this;
    }

    /**
     * @param string $key
     * @return Contact|null
     */
    public function getContact(string $key)
    {
        return isset($this->contacts[$key]) ? $this->contacts[$key] : null;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Domain
     */
    public function setUser(User $user): Domain
    {
        $this->user = $user;
        $this->user->addDomain($this);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCommands()
    {
        return $this->commands;
    }

    /**
     * @param Collection $commands
     * @return Domain
     */
    public function setCommands(Collection $commands)
    {
        $this->commands = $commands;
        return $this;
    }

    /**
     * @return int
     */
    public function getDnsZone()
    {
        return $this->dnsZone;
    }

    /**
     * @param int $dnsZone
     * @return Domain
     */
    public function setDnsZone(int $dnsZone): Domain
    {
        $this->dnsZone = $dnsZone;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->domainName;
    }

    /**
     * @return string
     */
    public function getLastGandiOperation()
    {
        return $this->lastGandiOperation;
    }

    /**
     * @param string $lastGandiOperation
     * @return Domain
     */
    public function setLastGandiOperation(string $lastGandiOperation): Domain
    {
        $this->lastGandiOperation = $lastGandiOperation;
        return $this;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        $domainArr = explode('.', $this->domainName, 2);
        return sizeof($domainArr) > 0 ? $domainArr[1] : null;
    }
}
