<?php

namespace AppBundle\Entity\Zone;

use Symfony\Component\Validator\Constraints as Assert;

class Record
{

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\Choice({"A", "AAAA", "CNAME", "MX", "NS", "TXT", "WKS", "SRV", "LOC", "SPF", "CAA"})
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $value;

    /**
     * @var int
     *
     */
    private $ttl;

    /**
     * @var Version
     */
    private $version;

    /**
     * Record constructor.
     * @param string|null $id
     * @param string|null $name
     * @param int|null $ttl
     * @param string|null $type
     * @param string|null $value
     */
    public function __construct(string $id = null, string $name = null, int $ttl = null, string $type = null, string $value = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->ttl = $ttl;
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Record
     */
    public function setId(string $id): Record
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Record
     */
    public function setName(string $name): Record
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Record
     */
    public function setType(string $type): Record
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Record
     */
    public function setValue(string $value): Record
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getTtl()
    {
        return $this->ttl;
    }

    /**
     * @param int $ttl
     * @return Record
     */
    public function setTtl(int $ttl): Record
    {
        $this->ttl = $ttl;
        return $this;
    }

    /**
     * @return Version
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param Version $version
     * @return Record
     */
    public function setVersion(Version $version): Record
    {
        $this->version = $version;
        return $this;
    }
}
