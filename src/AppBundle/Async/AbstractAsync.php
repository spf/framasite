<?php

namespace AppBundle\Async;

use AppBundle\Entity\User;
use Psr\Log\LoggerInterface;

abstract class AbstractAsync
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * The user that triggers this action
     *
     * @var User
     */
    protected $user;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Sets a logger instance on the object.
     *
     * @param LoggerInterface $logger
     * @return AbstractAsync
     */
    public function setLogger(LoggerInterface $logger): AbstractAsync
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * Set User
     * @param User $user
     * @return AbstractAsync
     */
    public function setUser(User $user): AbstractAsync
    {
        $this->user = $user;
        return $this;
    }
}
